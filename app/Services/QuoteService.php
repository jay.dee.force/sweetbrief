<?php 

namespace App\Services;

class QuoteService{

	//TODO: should be fed with database resource

	protected $quotes = [
		'Common! Get inspired...',
		'Start something from nothing, make them see something no one has seen before...',
		'Believe in art and all will flow...',
		'Gotcha, the world is a big place then...'
	];

	public function getRandomQuote(){
		return $this->quotes[array_rand($this->quotes, 1)];
	}

}