<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Story extends Model
{

	protected $fillable = ['content', 'user_id'];

    public function user(){
    	return $this->belongsTo(User::class);
    }

}
