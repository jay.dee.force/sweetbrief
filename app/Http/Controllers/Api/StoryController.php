<?php

namespace App\Http\Controllers\Api;

use App\Story;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\StoryResource;
use App\Http\Requests\StoreStoryRequest;
use App\Http\Resources\StoryResourceCollection;
use Illuminate\Support\Facades\Auth;

class StoryController extends Controller
{

	public function feed(){

		return StoryResource::collection(Story::latest()->paginate(15));
		
	}

    public function store(StoreStoryRequest $request){

    	$story = Story::create(array_merge(
                [
                    'user_id' => Auth::id()
                ],
                $request->only(['content'])
            )
        );

    	return response()->json([
    		'status' => 'success',
            'resource' => new StoryResource($story)
    	], 200);

    }
}
