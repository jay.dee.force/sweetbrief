<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>SweetBrief</title>
	<meta name="description" content="" />

	<meta name="keywords" content="">

	<meta property="og:title" content="" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="http://www.sweetbrief.com" />
	<!-- <meta property="og:image" content="http://sweetbrief.com/assets/logo-og.jpg" /> -->
	<meta property="og:description" content="" />

    <meta name="csrf-token" content="{{ csrf_token() }}">

	<!-- Global site tag (gtag.js) - Google Analytics -->
<!-- 	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-83490804-5"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-83490804-5');
	</script> -->

	<script>
        window.SweetBrief = {
        	state: "this"
        };
    </script>

	<link rel="stylesheet" href="/css/app.css">
	@stack('styles')
</head>
<body>

@include('layouts.header')

@yield('content')

<script src="/js/app.js"></script>

@stack('scripts')

</body>
</html>