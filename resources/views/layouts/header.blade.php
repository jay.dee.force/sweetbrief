<nav class="navbar navbar-expand-lg navbar-light sticky-top bg-white">
  <a class="navbar-brand" href="/"><h1>sweetbrief</h1></a>
  @if(Auth::check())
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <i class="fas fa-ellipsis-h"></i>
  </button>
  @endif
  
  @if(Auth::check())
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a class="nav-link" href="/">My Account</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/logout">Log out</a>
      </li>
    </ul>
  </div>
  @else
    <a class="nav-link" href="/login">Log in</a>
  @endif
</nav>